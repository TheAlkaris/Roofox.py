#!/usr/bin/python3
# -*- coding: utf-8 -*-

# /----------------------------------------------------------\
# | This script was created as a fun thing to learn how      |
# | to use Python 3 with Ncurses together in one script.     |
# | I don't expect anything siginificantly special out of    |
# | this script, but folks are welcome to adapt to this if   |
# | you want to play around with it, and learn from it maybe |
# \----------------------------------------------------------/


#include <curses.h>
#include <curses.h>
#include <signal.h>
#include <menu.h> -lmenu -lncurses

from os import system
import curses, traceback, string

# lets declare color set
def main(stdscr):
    curses.use_default_colors()
    for i in range(0, curses.COLORS):
        curses.init_pair(i, i, -1)
    try:
        for i in range(0,255):
            stdscr.addstr(str(i), curses.color_pair(i))
    except curses.ERR:
        # EOS
        pass
    stdscr.getch()
curses.wrapper(main)


def get_param(prompt_string):
    screen.clear()
    screen.border(0)
    screen.addstr(2, 2, prompt_string)
    screen.refresh()
    input = screen.getstr(10, 10, 60)
    return input

def execute_cmd(cmd_string):
    system("clear")
    a = system(cmd_string)
    print ()
    if a == 0:
        print ()
    else:
        print ("Command execution failure")

def main(stdscr):
    # Frame interface area at fixed VT100 size
    global screen
    screen = stdscr.subwin(23, 79, 0, 0)
    screen.box()
    screen.hline(2, 1, curses.ACS_HLINE, 77)
    screen.refresh()

# Define topbar menus // This was to draw a topbar with menus, but doesn't display
    file_menu = ("File", "file_func()")
    edit_menu = ("Edit", "edit_func()")
    help_menu = ("Help", "help_func()")

# Add topbar menus to screen object
    topbar_menu((file_menu, edit_menu, help_menu))

# Enter topbar menu loop
    while topbar_key_handler():
        draw_dict()
        
def file_func():
    s = curses.newwin(5,10,2,1)
    s.box()
    s.addstr(1,2, "I", hotkey_attr)
    s.addstr(1,3, "nput", menu_attr)
    s.addstr(2,2, "O", hotkey_attr)
    s.addstr(2,3, "utput", menu_attr)
    s.addstr(3,2, "T", hotkey_attr)
    s.addstr(3,3, "ype", menu_attr)
    s.addstr(1,2, "", hotkey_attr)
    s.refresh()
    c = s.getch()  
    if c in (ord('I'), ord('i'), curses.KEY_ENTER, 10):
        curses.echo()
        s.erase()
        screen.addstr(5,33, " "*43, curses.A_UNDERLINE)
        cfg_dict['source'] = screen.getstr(5,33)
        curses.noecho()
    else:
        curses.beep()
        s.erase()
    return CONTINUE

x = 0

while x != ord('6'):
    screen = curses.initscr()
# Name of options // ver. # 
    screen.clear()
    screen.border(0)
    screen.addstr(3, 85, "[ ./roofox.py Linux enthusiast ᕕ( ᐛ )ᕗ ]",
                  curses.A_BOLD)
    screen.addstr(2, 2, "[ What will you do with your Roofox? ]",
                  curses.A_BOLD)
    screen.addstr(2, 85, "[ Roofoxy.py v0.1b                     ]",
                  curses.A_BOLD)
    screen.addstr(4, 4, "1 - Pet the Roofox")
    screen.addstr(5, 4, "2 - Feed the Roofox")
    screen.addstr(6, 4, "3 - Snuggle the Roofox")
    screen.addstr(7, 4, "4 - Play with the Roofox")
    screen.addstr(8, 4, "5 - Eat the Roofox")
    screen.addstr(9, 4, "6 - Exit")
    screen.refresh()

    x = screen.getch()
                            # Number options
    if x == ord('1'):
        pet = get_param("** Pets the Roofoxes belly **")
        curses.endwin()
        execute_cmd(pet)
    if x == ord('2'):
        feed = get_param("** Feeds the Roofox some sushi **")
        curses.endwin()
        execute_cmd(feed)
    if x == ord('3'):
        snuggle = get_param("** Snuggles the Roofox **")
        curses.endwin()
        execute_cmd(snuggle)
    if x == ord('4'):
        play = get_param("** Plays with Roofox **")
        curses.endwin()
        execute_cmd(play)
    if x == ord('5'):
        eat = get_param("** You eat the Roofox **")
        curses.endwin()
        execute_cmd(eat)
    if x == ord('6'):
        curses.endwin()
        execute_cmd("echo '\033[1mRoofox.py\033[0m \033[7mexit\033[0m'")
curses.endwin()
