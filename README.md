Roofox.py
=========

Some random useless thing called *Roofox.py*
* * *
###### Requirements:
* Python 3
* ncurses

This _may work_ with *Python 2.7.x* but certain non-ASCII characters wont display correctly.

* * *
###### How to use:
There's not really anything to describe here on how to use. Just pick a number and get a resut output. There's only limited outputs. I was thinking of making something useful out of this, or just something fun with random responses, but it's rather difficult figuring out ncurses on my own to make things work, so for now it's just a basic input program.

be sure to do the following before you want to run this script;

```
chmod +x roofox.py
```

then follow up to run it by simply doing;

```
./roofox.py
```

* * *
###### TO-DO:
I want to create a windowed box using ncurses with it's own color instead of running with terminal's default background output.

![Roofox.py preview](/Shutter_2014_12_06__07:05:00.png "Roofox.py example")


To make something useful or fun out of this script.


To make the ncurses menu bar work, they don't appear to show up at all, but the script runs just fine.

